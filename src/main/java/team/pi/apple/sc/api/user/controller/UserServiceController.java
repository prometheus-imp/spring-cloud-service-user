package team.pi.apple.sc.api.user.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created on 31/10/2017 15:44
 *
 * @author edwin.zhang
 */
@RestController
@RequestMapping("/user")
public class UserServiceController {

    @Value("${demo.word:nothing-found}")
    String word;

    @RequestMapping("/detail")
    public String Detail() {
        return "Mock information of user. say " + word;
    }
}
